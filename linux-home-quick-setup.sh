#!/bin/sh
# Chris Zeigler 2021-05-25
#
# Make sure that you install CURL and Git first make sure that GitLab
# has SSH key
#
# curl -Lks https://gitlab.com/czeigler/linux-home-quick-setup/-/raw/master/linux-home-quick-setup.sh | /bin/sh

set -e
git_url=git@gitlab.com:czeigler/dotfiles.git


rm -rf "$HOME/.bash_logout" "$HOME/.bashrc" "$HOME/.profile" "$HOME/.zshrc" "$HOME/.gitconfig"  "$HOME/.bash_profile"  

if [ ! -f "$HOME/.ssh/id_rsa_gitlab_insecure" ]; then
    echo "[Error] ~/.ssh/id_rsa_gitlab_insecure does not exist!" 1>&2
else
    ssh-keyscan gitlab.com  >> ~/.ssh/known_hosts
    export GIT_SSH_COMMAND='ssh -i ~/.ssh/id_rsa_gitlab_insecure'

    alias config="git --git-dir=\$HOME/.cfg/ --work-tree=\$HOME"

    git clone --bare "$git_url"  "$HOME/.cfg"
    git --git-dir="$HOME/.cfg/" --work-tree="$HOME" checkout

    git --git-dir="$HOME/.cfg/" --work-tree="$HOME" config --local status.showUntrackedFiles no
    git --git-dir="$HOME/.cfg/" --work-tree="$HOME" config status.showUntrackedFiles no

    git config --global user.name "Chris Zeigler"
    git config --global user.email "czeigler@gmail.com"

fi
